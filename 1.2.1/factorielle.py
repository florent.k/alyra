


def factorielle(n):
    res = 1               # O(1)
    for i in range(n):
        res *= i+1        # O(n)
    return res            # O(1)


if __name__ == "__main__":
    for i in range(20):
        f = factorielle(i)
        print("{} > {}".format(i, f))
