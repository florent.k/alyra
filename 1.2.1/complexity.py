
import math
import matplotlib.pyplot as plt

f_x = []
f_y = []
f_z = []

for i in range(100):
    if i == 0:
        new_length = 1
    else:
        new_length = f_y[-1] + math.log10(i)

    print('{}: lengh:{}'.format(i, new_length))

    f_x.append(i)
    f_y.append(new_length)

plt.plot(f_x, f_y, 'ro')
plt.show()
