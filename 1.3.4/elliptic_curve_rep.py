import math


class EllipticCurve:
	def __init__(self, a, b):
		self.a = a
		self.b = b
		if 4*a**3+27*b**2 == 0:
			raise ValueError('(a={}, b={}) is not valid'.format(a, b))

	def __eq__(self, cuveToCompare):
		return self.a == cuveToCompare.a and self.b == cuveToCompare.b

	def testPoint(self,x, y):
		'''
		as the resolution (precision) of the number is limited, two expressions
		which are theorically equal can have very slightly different values if
		they aren't calculated the same way.
		for that reason, we decide to accept differences which are a few orders
		of magnitude the resolution (precision) of the number
		'''
		#print('y**2: {}'.format(y**2))
		#print('x**3 + self.a*x + self.b: {}'.format(x**3 + self.a*x + self.b))
		right = y**2
		left = x**3 + self.a*x + self.b
		return abs(right - left) < 10e-10

	def strDef(self):
		return 'Elliptic Curve: (a={}, b={})'.format(self.a, self.b)


curve_1 = EllipticCurve(0, 7)
curve_2 = EllipticCurve(0, 7)
curve_3 = EllipticCurve(0, 7.0001)

print('curve_1: ' + curve_1.strDef())
print('curve_2: ' + curve_2.strDef())
print('curve_3: ' + curve_3.strDef())

print('curve_1 == curve_2 ? : ' + str(curve_1 == curve_2))
print('curve_1 == curve_3 ? : ' + str(curve_1 == curve_3))

x = 1
y = math.sqrt(8)
print('point ({},{}) on curve_1 ? : {}'.format(x,y,curve_1.testPoint(x,y)))

x = 2
y = math.sqrt(8)
print('point ({},{}) on curve_1 ? : {}'.format(x,y,curve_1.testPoint(x,y)))
