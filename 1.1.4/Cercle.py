

import math

class Cercle():
    def __init__(self, rayon):
        self.rayon = rayon

    def aire(self):
        return math.pi * math.pow((self.rayon), 2)

    def perimetre(self):
        return 2 * self.rayon * math.pi

if __name__ == "__main__":
    c = Cercle(1)
    print("Cercle de rayon 1:")
    print(c.perimetre())
    print(c.aire())
