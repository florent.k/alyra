

from math import floor


def estPalindrome(text):
    textClr = ''.join(l for l in text if l not in ' .!?').lower()

    ctrlRange = floor(len(textClr)/2)
    for i in range(ctrlRange):
        if textClr[i] != textClr[-(i+1)]:
            print(textClr)
            print(' '*i + '^' + ' '*(len(textClr)-(2*i)-2) + '^')
            return False
    return True


if __name__ == "__main__":
    text = input("Entrez un texte: ")

    if estPalindrome(text):
        print("==> palindrôme")
    else:
        print("==> PAS un palindrôme")
