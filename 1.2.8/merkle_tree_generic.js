
const crypto = require('crypto')

const calc_hash = function(input, nb_char=8) {
  h = crypto.createHash('sha256').update(input).digest('hex')
  return h.substring(0, nb_char-1)
}


class MerkleTree {
  constructor(input) {
    this.input = input
    this.tree = {}
    this.depth = this.merkleDepth()

    // create empty datastructure for the Merkle Tree
    for(var i=0; i<this.depth; i++) {
      this.tree[i] = []
    }

    // create the first line of the tree using the inputs
    for(var entry in input) {
      var entry_hash = calc_hash(input[entry])
      this.tree[0].push(entry_hash)
    }

    // create all following line until the final hash
    var depth = 0
    while(this.tree[depth].length > 1) {
      var last_hash = ''
      for(var i=0; i<this.tree[depth].length; i++) {
        if(i == this.tree[depth].length-1 && i % 2 == 0) {
          var new_hash = calc_hash(this.tree[depth][i])
          this.tree[depth+1].push(new_hash)
        } else if (i%2 == 0) {
          last_hash = this.tree[depth][i]
        } else {
          var new_str = last_hash + this.tree[depth][i]
          var new_hash = calc_hash(new_str)
          this.tree[depth+1].push(new_hash)
        }
      }
      depth += 1
    }
  }

  merkleDepth() {
    var input_size = Object.keys(this.input).length
    for(var n=0; Math.pow(2,n) < input_size; n++) {}
    return n+1
  }

  getMerkleHead() {
    return this.tree[this.depth-1]
  }

  isInTree(to_verify) {
    // check that hash of entry is in tree
    var hash_to_verify = calc_hash(to_verify)
    var index = this.tree[0].indexOf(hash_to_verify)
    if(index == -1) {
      console.log('entry has no corresponding hash in tree')
      console.log('calculated: ' + hash_to_verify)
      return false
    }

    // check consistency by navigating to head of tree
    for(var depth = 0; depth < this.depth-1; depth++) {
      var index_parent = Math.floor(index/2)

      var hash_current = this.tree[depth][index]

      if(index == this.tree[depth].length-1 && index%2 == 0) {
        var str_parent = hash_current
        var hash_parent_calc = calc_hash(str_parent)
        console.log(hash_current + ' => ' + hash_parent_calc)
      } else {
        if(index%2 == 0) {
          var hash_bro = this.tree[depth][index+1]
          var str_parent = hash_current + hash_bro
        } else {
          var hash_bro = this.tree[depth][index-1]
          var str_parent = hash_bro + hash_current
        }
        var hash_parent_calc = calc_hash(str_parent)
        console.log(hash_current + ' + ' + hash_bro + ' => ' + hash_parent_calc)
      }
      var hash_parent_read = this.tree[depth+1][index_parent]

      if(hash_parent_calc != hash_parent_read) {
        console.log('calculated hash does not match tree; ' + hash_current +
                    ' + ' + hash_bro + ' != ' + hash_parent_read)
        return false
      }
      index = index_parent
    }
    return true
  }

}


input_data = {}
input_data.A = 'mouton'
input_data.B = 'chat'
input_data.C = 'agneau'
input_data.D = 'vache'
input_data.E = 'elephant'

var merkleTree = new MerkleTree(input_data)

console.log('Merkle Tree:')
console.log(merkleTree.tree)

console.log('\nHead of the tree:')
console.log(merkleTree.getMerkleHead())


var str_list = ['agneau', 'elephant', 'elefant']
for(var i in str_list) {
  str = str_list[i]
  console.log('\nChecking that the string is in the Merkle Tree: ' + str)
  console.log(merkleTree.isInTree(str))
}
