

const crypto = require('crypto')

const calc_hash = function(input, nb_char=8) {
  h = crypto.createHash('sha256').update(input).digest('hex')
  return h.substring(0, nb_char-1)
}


input_data = {}
input_data.A = 'kdjfsljféldsjséldsfoiewqjfésdoijfsaodifjsaodfjsaé'
input_data.B = 'j932872382763782728130420843984093804309430948304'
input_data.C = 'KDSJWIEOIWJMEXLIWJFDLMWEK2LKEJDLKEJHDSLKJEALKALJS'
input_data.D = ')(/()&(("/*)(*=)=/)*=)"/=)*)(*&%%"%Ç"ÇÇ/&/&/&())1'


hash = {}
hash.A = calc_hash(input_data.A)
hash.B = calc_hash(input_data.B)
hash.C = calc_hash(input_data.C)
hash.D = calc_hash(input_data.D)

hash.AB = calc_hash(hash.A + hash.B)
hash.CD = calc_hash(hash.C + hash.D)

hash.ABCD = calc_hash(hash.AB + hash.CD)


arr = ['A', 'B', 'C', 'D']
for (var e in arr) {
  process.stdout.write(arr[e] + ':' + hash[arr[e]] + '  ')
}

console.log('')

arr = ['AB', 'CD']
for (var e in arr) {
  process.stdout.write(arr[e] + ':' + hash[arr[e]] + '  ')
}

console.log('')

console.log('ABCD:'+hash.ABCD)
