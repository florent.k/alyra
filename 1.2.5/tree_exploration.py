# coding=utf8


class Noeud:
    def __init__(self, v, parent):
        self.parent = parent
        self.gauche = None
        self.droite = None
        self.valeur = v

    def affiche(self):
        print('\n')
        print('valeur: {}'.format(self.valeur))
        print('objet: {}'.format(self))
        print('parent: {}'.format(self.parent))
        print('gauche: {}'.format(self.gauche))
        print('droite: {}'.format(self.droite))




class Arbre:
    def __init__(self):
        self.racine = None

    def ajouter(self, val):
        if(self.racine == None):
            self.racine = Noeud(val, None)
        else:
            self._ajouter(val, self.racine)

    def _ajouter(self, val, nd):
        if(val < nd.valeur):
            if(nd.gauche is not None):
                self._ajouter(val, nd.gauche)
            else:
                nd.gauche = Noeud(val, nd)
        else:
            if(nd.droite is not None):
                self._ajouter(val, nd.droite)
            else:
                nd.droite = Noeud(val, nd)

    def cherche(self, val):
        if(self.racine == None):
            return False
        else:
            return self._cherche(val, self.racine)

    def _cherche(self, val, nd):
        if(val == nd.valeur):
            return nd
        elif(val < nd.valeur):
            if(nd.gauche is not None):
                return self._cherche(val, nd.gauche)
            elif(nd.valeur == val):
                return nd
            else:
                return None
        else:
            if(nd.droite is not None):
                return self._cherche(val, nd.droite)
            elif(nd.valeur == val):
                return nd
            else:
                return None

    def affiche(self):
        if(self.racine == None):
            print('empty tree')
        else:
            self._affiche(self.racine, 0)

    def _affiche(self, nd, iter):
        if nd.gauche:
            self._affiche(nd.gauche, iter+1)
        print('   '*iter + str(nd.valeur) )
        if nd.droite:
            self._affiche(nd.droite, iter+1)


    def supprime_nd(self, nd):
        if nd.droite == None and nd.gauche == None:
            if nd.parent == None:
                self.racine = None
            elif nd == nd.parent.gauche:
                nd.parent.gauche = None
            else:
                nd.parent.droite = None
        elif nd.gauche == None:
            if nd.parent == None:
                self.racine = nd.droite
            elif nd == nd.parent.gauche:
                nd.parent.gauche = nd.droite
                nd.droite.parent = nd.parent
            else:
                nd.parent.droite = nd.droite
                nd.droite.parent = nd.parent
        elif nd.droite == None:
            if nd.parent == None:
                self.racine = nd.gauche
            elif nd == nd.parent.gauche:
                nd.parent.gauche = nd.gauche
                nd.gauche.parent = nd.parent
            else:
                nd.parent.droite = nd.gauche
                nd.gauche.parent = nd.parent
        else:
            self.supprime_nd_2bran(nd)

    def supprime_nd_2bran(self, nd):
        nd_srch_g = nd.gauche
        while nd_srch_g.droite != None:
            nd_srch_g = nd_srch_g.droite

        nd_srch_d = nd.droite
        while nd_srch_d.gauche != None:
            nd_srch_d = nd_srch_d.gauche

        dist_d = abs(nd_srch_d.valeur - nd.valeur)
        dist_g = abs(nd.valeur - nd_srch_g.valeur)
        if dist_d < dist_g:
            nd_srch_d.gauche = nd.gauche
            nd.gauche.parent = nd_srch_d
            if nd.parent != None:
                if nd.parent.gauche == nd:
                    nd.parent.gauche = nd.droite
                    nd.droite.parent = nd.parent
                else:
                    nd.parent.droite = nd.droite
                    nd.droite.parent = nd.parent
            else:
                self.racine = nd.droite
                nd.droite.parent = None
        else:
            nd_srch_g.droite = nd.droite
            nd.droite.parent = nd_srch_g
            if nd.parent != None:
                if nd.parent.gauche == nd:
                    nd.parent.gauche = nd.gauche
                    nd.gauche.parent = nd.parent
                else:
                    nd.parent.droite = nd.gauche
                    nd.gauche.parent = nd.parent
            else:
                self.racine = nd.gauche
                nd.gauche.parent = None









import random

arbre = Arbre()

for n in [38, 43, 2, 99, 37, 52, 33, -10, 101, 943, 40, 41]:
    arbre.ajouter(n)

i =  52
print("\n#### recherche de la valeur {} dans l'arbre".format(i))
if(arbre.cherche(i) is not None):
    print('found')
else:
    print('not found')


print('\n#### arbre original')
arbre.affiche()


i = 43
print('\n#### arbre après effaçage du noeud {}'.format(i))
nd = arbre.cherche(i)
arbre.supprime_nd(nd)
arbre.affiche()

i = 99
print('\n#### arbre après effaçage du noeud {}'.format(i))
nd = arbre.cherche(i)
arbre.supprime_nd(nd)
arbre.affiche()


i = 33
print('\n#### arbre après effaçage de la feuille {}'.format(i))
nd = arbre.cherche(i)
arbre.supprime_nd(nd)
arbre.affiche()

i = 41
print('\n#### arbre après effaçage du noeud {}'.format(i))
nd = arbre.cherche(i)
arbre.supprime_nd(nd)
arbre.affiche()
