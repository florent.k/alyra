# coding=utf8

def letter_occurencies(input_str):
    input_str = input_str.lower()
    num_list = []
    occur = {}

    for char in input_str:
        num_list.append(ord(char))

    for num in num_list:
        if num in occur.keys():
            occur[num] += 1
        else:
            occur[num] = 1

    print('Occurencies of: "{}"'.format(input_str))
    for num in sorted(occur.keys()):
        print('{} : {}x'.format(chr(num), occur[num]))


letter_occurencies('hello world')
letter_occurencies('Etre contesté, c’est être constaté')
