# coding=utf8

from math import floor


NB_MIN_VAL = 1
NB_MAX_VAL = 100



def exit_error(msg):
    print(msg)
    exit()


def get_nb_to_guess(min_val, max_val):
    print("Entez un nombre entre {} et {}".format(min_val, max_val))

    nb = input()

    try:
        nb_to_guess = int(nb)
    except:
        exit_error("Erreur: le nombre entré n'est pas un entier")

    if nb_to_guess > max_val or nb_to_guess < min_val:
        exit_error("Erreur: le nombre à trouver doit être compris entre 1 et 100")

    return nb_to_guess



nb_to_guess = get_nb_to_guess(NB_MIN_VAL, NB_MAX_VAL)

min_val = NB_MIN_VAL # variables are updated according to the result of the guesses
max_val = NB_MAX_VAL
nb_iter = 0

while True:
    nb_iter += 1
    guess = floor((max_val - min_val)/2 + min_val)

    print("Itération: {} ; Essai: {}".format(nb_iter, guess))

    if guess == nb_to_guess: # the guess takes place at this moment
        print("Trouvé !")
        break
    elif guess > nb_to_guess:
        max_val = guess
    else:
        min_val = guess
