
def caesar_cypher(input_str, shift):
    input_str = input_str.lower()

    list_orig = []
    list_new = []
    result = ''

    for char in input_str:
        list_orig.append(ord(char)-ord('a'))

    for c in list_orig:
        list_new.append((c+shift)%26)

    for c in list_new:
        result += chr(c+ord('a'))

    print('\nConverting: ' + input_str)
    print('Original nb format: ' + str(list_orig))
    print('Cyphered nb format: ' + str(list_new))
    print('Cyphered str result: ' + str(result))

    return result


coded_msg = caesar_cypher('hello', 3)
decoded_msg = caesar_cypher(coded_msg, -3)
print('\nDecoded message: ' + decoded_msg)
