# coding=utf8


import math
import pprint


'''****************************************************************************
                    Complexity analysis of bruteforce method
****************************************************************************'''

def binom_coef(x, y):
    '''
    from: https://stackoverflow.com/questions/26560726/python-binomial-coefficient
    '''
    if y == x:
        return 1
    elif y == 1:
        return x
    elif y > x:
        return 0
    else:
        a = math.factorial(x)
        b = math.factorial(y)
        c = math.factorial(x-y)
        return a // (b * c)

def nb_combinations(max_nb_transac):
    '''
    max_nb_tr : maximal number of transactions
    we actually find out that the result is always 2^n
    because this is a process in which we choose to to take or not each transaction
        therfore, there are 8 times 2 choices to do
    see also: https://en.wikipedia.org/wiki/Binomial_coefficient#Sums_of_the_binomial_coefficients
    '''
    print('Analysis of the total total number of combinations:')
    nb_tot_combination = 0
    for i in range(max_nb_transac+1):
        nb_combination = binom_coef(max_nb_transac,i)
        nb_tot_combination += nb_combination
        print('({} {}) = {}'.format(max_nb_transac, i, nb_combination))

    print('nb_comb: {}\n'.format(nb_tot_combination))
    return nb_tot_combination

nb_combinations(8) # = 2^8 = 256

'''
nb of combinations:  2^n
computing complexity of one combination: O(n)
    for each selected transaction, sum up all the sizes and reward values
=> Complexity of the bruteforce algorithm: O(n * 2^n)
'''



'''****************************************************************************
                    Implementation of bruteforce method
****************************************************************************'''


class Combin():
    def __init__(self, transacs):
        self.transacs = transacs
        self.size = 0
        self.reward = 0

        for transac in self.transacs:
            self.size += transac['size']
            self.reward += transac['reward']

    def show(self):
        pprint.pprint(self.transacs)
        print('reward: {}'.format(self.reward))
        print('size: {}'.format(self.size))


def get_best_combination(transac, max_size):
    best_combin = Combin([])

    nb_tot_combin = int(math.pow(2,len(transac)))

    for combin_nb in range(nb_tot_combin):
        sel_transacs = []

        sel_binary_rep = "{0:b}".format(combin_nb).zfill(len(transac))
        for transac_index, item in enumerate(transac):
            if sel_binary_rep[transac_index] == '1':
                sel_transacs.append(transac[transac_index])

        sel_combin = Combin(sel_transacs)

        if sel_combin.size <= max_size and sel_combin.reward > best_combin.reward:
            best_combin = sel_combin

    return best_combin


### particular test

transacs = [
        {'size': 2000, 'reward':  13000},
        {'size': 6000, 'reward':  9000},
        {'size': 800, 'reward':  2000},
        {'size': 700, 'reward':  1500},
        {'size': 1200, 'reward':  3500},
        {'size': 1000, 'reward':  2800},
        {'size': 1300, 'reward':  5000},
        {'size': 600, 'reward':  1500}
    ]


print('Best bruteforce combination:')
best_combin = get_best_combination(transacs, 6000)
best_combin.show()
