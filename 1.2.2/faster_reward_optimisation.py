# coding=utf8

'''****************************************************************************
                        Faster reward optimization
****************************************************************************'''

'''
stratégie:
  - trier toutes les transactions par orde de rentabilité (récompense/taille)
    > complexité: O(n.log(n)) si fonction de triage optimisé par python.
        see: https://stackoverflow.com/questions/14434490/
  - ajouter les transactions les plus rentables en premier, jusqu'à ce qu'il
    n'y aie plus de place.
    > complexité: O(n)
  - Pour un nombre fixe de fois (par exemple 10 fois), prend le premier bloc le
    plus rentable qui n'a pas pu rentrer dans la sélection, et enlève les blocs
    les moins rentables jusqu'à ce qu'il y aie assez de place pour l'intégrer.
    Compare avec l'état précédent et le sélectione si il apporte une plus
    grande récompense.
    > complexité: O(n)

    Complexité totale: O(n.log(n))
'''

import pprint
import copy

class Combin():
    def __init__(self, transacs):
        self.transacs = transacs
        self.size = 0
        self.reward = 0

        for transac in self.transacs:
            self.size += transac['size']
            self.reward += transac['reward']

    def add_transac(self, transac):
        self.transacs.append(transac)
        self.size += transac['size']
        self.reward += transac['reward']

    def pop_last_transac(self):
        transac = self.transacs.pop()
        self.size -= transac['size']
        self.reward -= transac['reward']

    def show(self):
        pprint.pprint(self.transacs)
        print('reward: {}'.format(self.reward))
        print('size: {}'.format(self.size))


def get_best_combination(transacs, max_size, nb_optim_iter):
    # sort transactions by profitabiltiy
    sorted_open_transacs = sorted(transacs, key=lambda k: -k['reward']/k['size'])

    # add as many as possible in order of profitabiltiy staying below max_size
    selected_combin = Combin([])
    while sorted_open_transacs:
        if selected_combin.size + sorted_open_transacs[0]['size'] <= max_size:
            selected_combin.add_transac(sorted_open_transacs.pop(0))
        else:
            break

    # optional extended optimization
    for iter in range(nb_optim_iter):
        if not sorted_open_transacs: break
        transac_to_add = sorted_open_transacs.pop(0)

        alternative_combin = copy.copy(selected_combin)

        while alternative_combin.transacs:
            if transac_to_add['size'] <= (max_size - alternative_combin.size):
                alternative_combin.add_transac(transac_to_add)
                break
            alternative_combin.pop_last_transac()

        if alternative_combin.reward > selected_combin.reward:
            selected_combin = alternative_combin

    return selected_combin


### particular test


# example from the exercise

transacs = [
        {'size': 2000, 'reward':  13000},
        {'size': 6000, 'reward':  9000},
        {'size': 800, 'reward':  2000},
        {'size': 700, 'reward':  1500},
        {'size': 1200, 'reward':  3500},
        {'size': 1000, 'reward':  2800},
        {'size': 1300, 'reward':  5000},
        {'size': 600, 'reward':  1500}
    ]

max_size = 6000
nb_optim_iter = 0

'''
# example where the extended optimization is useful

transacs = [
        {'size': 1000, 'reward':  2000}, # most interesting transaction
        {'size': 100, 'reward':  100},   # good reward/size ration, but does not
                                         # allow the next transaction which
                                         # gives a higher reward
        {'size': 1000, 'reward':  500}, # lower reward/size ration, but fits
                                         # perfectly and big reward
    ]

max_size = 2000
nb_optim_iter = 3
'''

print('Selected bruteforce combination:')
selected_combin = get_best_combination(transacs, max_size, nb_optim_iter)
selected_combin.show()
