
# coding=utf8

from random import randint

nb_to_find = randint(1, 100)

print("Essayez de devinner la valeur du nombre:")

while True:
    guess = input()

    try:
        guess = int(guess)
    except:
        print("Erreur: le nombre entré n'est pas un entier")
        exit()

    if guess == nb_to_find:
        print("Trouvé !")
        break
    elif guess >= nb_to_find:
        print("Trop grand ! ",end="")
    else:
        print("Trop petit ! ",end="")

    dist = abs(guess-nb_to_find)
    if dist <= 5:
        print("Mais tu es très proche  (<=5)")
    elif dist <=10:
        print("Mais tu es proche (6 à 10)")
    else:
        print("Tu es encore loin (plus de 10)")
